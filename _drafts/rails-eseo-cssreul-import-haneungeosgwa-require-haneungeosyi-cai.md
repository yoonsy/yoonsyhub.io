---
layout: post
status: draft
title: Rails 에서 css를 @import 하는것과 require 하는것의 차이
author:
  display_name: yoonsy
  login: nscorpio16
  email: nscorpio16@gmail.com
  url: http://yoonsy.kr
author_login: nscorpio16
author_email: nscorpio16@gmail.com
author_url: http://yoonsy.kr
wordpress_id: 457
wordpress_url: http://yoonsy.kr/?p=457
date: '2014-03-13 12:24:37 +0900'
categories:
- "미분류"
tags: []
comments: []
---
<p>https://github.com/rails/sass-rails#important-note</p></p>
<p>Sprockets provides some <a href="https://github.com/sstephenson/sprockets#the-directive-processor">directives</a> that are placed inside of comments called require, require_tree, and require_self. DO NOT USE THEM IN YOUR SASS/SCSS FILES. They are very primitive and do not work well with Sass files. Instead, use Sass's native @import directive which sass-rails has customized to integrate with the conventions of your Rails projects.</p></p>
<p><a href="https://github.com/chriseppstein/compass/tree/master/import-once">import once plugin by compass</a></p></p>
